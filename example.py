# -*- coding: utf-8 -*-
"""
Created on Tue Mar 28 16:55:41 2023

@author: rpiso
"""

import numpy as np
def model(inputs):
    result=np.zeros(4)
    result[0]=inputs[1]+inputs[2]*inputs[3]-inputs[4]-inputs[5]*inputs[0]
    result[1]=inputs[1]+inputs[2]*inputs[3]
    result[2]=inputs[6]
    result[3]=inputs[6]+inputs[2]*inputs[3]
    return result