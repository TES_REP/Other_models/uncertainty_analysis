# -*- coding: utf-8 -*-
"""
Created on Wed Mar  1 15:07:19 2023

@author: rpiso
"""

import numpy as np
from scipy.stats import norm, lognorm, uniform
from scipy.stats import t
from scipy.optimize import minimize
import scipy.stats as stats
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import lsq_linear

#YOUR MODEL:
from example import model

#########################MODEL
def fmod(x,n_ouputs):
    result=np.zeros((len(x),n_outputs))
    for i in range (0,len(x)):
        result[i]=model(x[i])
    return result

#########################UNCERTAINTY ANALYSIS
def Uncertainty_sensitivity(ym):
    D = np.var(ym)
    f0 = np.mean(ym)
    #start of the script for building linear models on the MC outputs
    n, m = X.shape
    
    if f0 ==0:
        return [f0,0,0,0,0], np.zeros(m),1
    # Step 4. Review and analyse the results
    MCerr = np.std(ym)/np.sqrt(N)
    
    #Confidence interval
    SEM = np.std(ym) / np.sqrt(len(ym))
    ts = stats.t.ppf([0.025, 0.975], len(ym)-1)
    CI = f0 + ts * SEM
    
    #1. Make the data dimensionless (auto scaling)
    Sim = ym
    Sim = ym.reshape(-1, 1)
    msim = np.mean(Sim)
    ssim = np.std(Sim)
    mx = np.mean(X, axis=0)
    sx = np.std(X, axis=0)
    
    
    #scale model outputs
    mus = np.ones((n, 1)) * msim
    sds = np.ones((n, 1)) * ssim
    Simsc = (Sim - mus) / sds
    
    #scale parameters
    muscale = np.ones((n, 1)) * mx
    sdscale = np.ones((n, 1)) * sx
    thetasc = (X - muscale) / sdscale
    jj, k = Sim.shape
    
    #2. perform linear regression FOR EACH MODEL OUTPUT
    
    src = np.zeros((m, k)) # standardized regression coefficients
    srtx = np.zeros((m, k)) # sorted parameters
    ii = np.zeros((m, k), dtype=int)
    rank = np.zeros((m, k), dtype=int) # ranking of parameters (according higher order of significance)
    cov_ = np.zeros((m, m, k)) # covariance of the coefficients of the linear model
    cor_ = np.zeros((m, m, k)) # correlation btw the coefficients of the linear model
    lb = np.ones((m, 1)) * -1 # SRC can take a value between -1 and 1
    ub = np.ones((m, 1)) * 1
    
    Rsq = np.zeros(k)
    unity = np.zeros(k)
    
    for j in range(k):
        dd = (ym - np.mean(ym)) / np.std(ym)
        print('SRC number:', j)
        b = lsq_linear(thetasc, dd, bounds=(lb.T[0], ub.T[0])).x
        y = thetasc @ b
        R = np.corrcoef(Simsc[:, j], y)[0, 1]
        Rsq[j] = R ** 2
        src[:, j] = b
        idx = np.argsort(np.abs(b))[::-1]
        srtx[:, j] = np.abs(b[idx])
        ii[:, j] = idx
        rank[idx, j] = np.arange(1, m+1)
        # below provides an estimate of covariance matrix also the
        # correlation matrix
        s2 = (sum(dd ** 2) / (n - m))
        cova = s2 * np.linalg.inv(thetasc.T @ thetasc) # covariance matrix
        for kk in range(m):
            for ll in range(m):
                core = cova[kk, ll] / (np.sqrt(cova[kk, kk]) * np.sqrt(cova[ll, ll]))
        cor_[kk, ll, j] = core
        cov_[:, :, j] = cova
        # the sum of all the src should be 1 (incase parameters are independent), lets check
        unity = b @ b
        plt.figure()
        plt.plot(Simsc[:, j], y, '.')
        imax = np.argmax(y)
        plt
    
    # Plot/view sampling results
    fig, ax = plt.subplots(figsize=(10, 10))
    ax = plt.imshow(np.corrcoef(X.T), cmap='coolwarm', interpolation='nearest')
    plt.colorbar(ax.colorbar, fraction=0.045)
    ax.colorbar.set_label('Correlation coefficient', fontsize=18)
    plt.xticks(np.arange(len(inputs)), inputs['Variable'], fontsize=16)
    plt.yticks(np.arange(len(inputs)), inputs['Variable'], fontsize=16)
    plt.title('Correlation matrix', fontsize=20)
    plt.show()
    
    # Plot histogram of model outputs
    fig, ax = plt.subplots(figsize=(10, 10))
    ax = plt.hist(ym, bins=50)
    plt.xlabel('f(x)', fontsize=18)
    plt.ylabel('Number of occurrences', fontsize=18)
    plt.xticks(fontsize=16)
    plt
    
    #tabulate results
    Uncertainty= pd.DataFrame()
    Uncertainty['mean'] = [f0]
    Uncertainty['Variance'] = D
    Uncertainty['MCerr'] = MCerr
    Uncertainty['CI lower']=CI[0]
    Uncertainty['CI upper']=CI[1]
    
    Sensitivity= pd.DataFrame()
    Sensitivity['Variable']=inputs['Variable']
    Sensitivity['SRC']=src
    
    return Uncertainty.iloc[0].values,src.T[0],Rsq

######################MAIN PROGRAM
#Step 1. Input uncertainty definition
direc='C:/Users/rpiso/Desktop/PhD_Python_files/Uncertainty_Analysis/'
data_parameters=pd.read_csv(direc + 'parameters_uncertainty.csv', sep=';')
n_outputs=4

# Step 2. Sampling
m = len(data_parameters)
N = 10000
Xp = np.random.rand(N, m)

# Latin Hypercube Sampling
# for i in range(m):
#     Xp[:, i] = (Xp[:, i] + np.arange(N)) / N

# Convert probability space to real value space via inverse distribution
# ADAPT TO DISTRIBUTION TYPE
#note thath norm.ppf uses mean and stdev and uniform.pff uses lower value and width
X = np.zeros((N, m))
inputs= pd.DataFrame(columns=['Variable','mu','sd'])
inputs['Variable']=data_parameters['Parameter']
for i in range(0,len(data_parameters)):
    if data_parameters['Type of deviation'][i]=='Uniform, absolute':
        inputs['mu'][i]=data_parameters['Mean value'][i]-0.5*data_parameters['Deviation'][i]
        inputs['sd'][i]=data_parameters['Deviation'][i]
        X[:, i] = uniform.ppf(Xp[:, i], inputs['mu'][i], inputs['sd'][i])
    else:
        inputs['mu'][i]=data_parameters['Mean value'][i]
        inputs['sd'][i]=data_parameters['Deviation'][i]
        X[:, i] = norm.ppf(Xp[:, i], inputs['mu'][i], inputs['sd'][i])

# Step 3. Run the model 
ym = fmod(X,n_outputs)

# Step 4. Uncertainty analysis
array_uncertainty=pd.DataFrame(columns=['mean','Variance','MCerr','CI lower','CI upper'])
array_sensitivity=pd.DataFrame(columns=inputs['Variable'])
array_Rsq=pd.DataFrame(columns=['Rsq'])
for i in range(0,n_outputs):    
    r=Uncertainty_sensitivity(ym[:,i])
    array_uncertainty.loc[i]=r[0]
    array_sensitivity.loc[i]=r[1]
    array_Rsq.loc[i]=r[2]

print(array_uncertainty)